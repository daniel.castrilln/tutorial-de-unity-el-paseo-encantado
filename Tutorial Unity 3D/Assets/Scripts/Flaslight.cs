using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flaslight : MonoBehaviour
{
    public Transform linterna;
    public float rango;
    public int pilas_C;
    public GameObject flashLight;
    //public Material mat;

    void Start()
    {
        pilas_C = 0;
        flashLight.SetActive(false);
    }

    void Update()
    {
        Debug.DrawRay(linterna.position, linterna.forward, Color.red);
        RaycastHit hit;
        if (Physics.Raycast(linterna.position, linterna.forward, out hit, rango))
        {
            if (Input.GetKeyDown("e") && pilas_C > 0)
            {
                if (hit.transform.tag == "Enemigo")
                {
                    Debug.Log("Enemigo");
                    hit.collider.gameObject.SetActive(false);
                    pilas_C--;
                    /*mat = hit.collider.GetComponent<Renderer>().material;
                    mat.DisableKeyword("_EMISSION"); Esto se utiliza para acceder a componentes que sean materiales o similiares     */
                }
                TurnOn();
                Debug.Log(hit.collider.name);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bateria")
        {
            other.gameObject.SetActive(false);
            pilas_C++;
        }
    }

    void TurnOn()
    {
        flashLight.SetActive(true);
        Invoke("TurnOff", 3f);
    }

    void TurnOff()
    {
        flashLight.SetActive(false);
    }





}
